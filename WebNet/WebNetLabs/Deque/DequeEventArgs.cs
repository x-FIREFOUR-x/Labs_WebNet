﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Deque
{
    public class DequeEventArgs: EventArgs
    {
        public string Message { get; set; }
    }
}

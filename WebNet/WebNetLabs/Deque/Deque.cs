﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace Deque
{
    public class Deque<T> : ICollection, ICloneable
    {
        DoublyNode<T> head;
        DoublyNode<T> tail;
        int count;

        public event EventHandler<DequeEventArgs> DequeEvent;

        public Deque()
        {
            head = null;
            tail = null;
            count = 0;
        }

        public void AddFirst(T value)
        {
            DoublyNode<T> newNode = new DoublyNode<T>(value);
            DoublyNode<T> temp = head;

            newNode.Next = temp;
            newNode.Previous = null;

            head = newNode;
            if (count == 0)
                tail = head;
            else
                temp.Previous = newNode;

            count++;

            DequeEvent?.Invoke(this, new DequeEventArgs{ Message = "Added first, size: " + count.ToString()});
        }

        public void AddLast(T value)
        {
            DoublyNode<T> newNode = new DoublyNode<T>(value);

            if (count == 0)
                head = newNode;
            else
            {
                tail.Next = newNode;
                newNode.Previous = tail;
            }
            tail = newNode;
            count++;

            DequeEvent?.Invoke(this, new DequeEventArgs { Message = "Added last, size: " + count.ToString() });
        }

        public T RemoveFirst()
        {
            if (count == 0)
                throw new InvalidOperationException();

            DoublyNode<T> temp = head;
            if (count == 1)
            {
                head = null;
                tail = null;
            }
            else
            {
                head = head.Next;
                head.Previous = null;
            }
            count--;

            DequeEvent?.Invoke(this, new DequeEventArgs { Message = "Deleted first, size: " + count.ToString() });

            return temp.Value;
        }

        public T RemoveLast()
        {
            if (count == 0)
                throw new InvalidOperationException();

            DoublyNode<T> temp = tail;
            if (count == 1)
            {
                head = null;
                tail = null;
            }
            else
            {
                tail = tail.Previous;
                tail.Next = null;
            }
            count--;

            DequeEvent?.Invoke(this, new DequeEventArgs { Message = "Deleted last, size: " + count.ToString()});

            return temp.Value;
        }

        public int Count { get { return count; } }
        public bool IsEmpty { get { return count == 0; } }

        public T First
        {
            get
            {
                if (IsEmpty)
                    throw new InvalidOperationException();
                return head.Value;
            }
        }
        public T Last
        {
            get
            {
                if (IsEmpty)
                    throw new InvalidOperationException();
                return tail.Value;
            }
        }

        public bool IsSynchronized => false;
        public object SyncRoot => this;

        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;

            DequeEvent?.Invoke(this, new DequeEventArgs { Message = "Cleaned, size: " + count.ToString() });
        }

        public bool Contains(T value)
        {
            DoublyNode<T> currentNode = head;

            while (currentNode != null)
            {
                if (currentNode.Value.Equals(value))
                    return true;

                currentNode = currentNode.Next;
            }
            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (head == null)
                yield break;

            DoublyNode<T> currentNode = head;
            while (currentNode != null)
            {
                yield return currentNode.Value;

                currentNode = currentNode.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public object Clone()
        {
            Deque<T> newDeque = new Deque<T>();
            foreach (var elem in this)
            {
                newDeque.AddLast(elem);
            }
            return newDeque;
        }

        public void CopyTo(Array array, int index)
        {
            if (array == null)
                throw new ArgumentNullException();
            if (index < 0)
                throw new ArgumentOutOfRangeException();
            if (array.Rank > 1)
                throw new ArgumentException();
            if (Count > array.Length - index)
                throw new ArgumentException();

            foreach (var elem in this)
            {
                array.SetValue(elem, index);
                index++;
            }

        }

    }
}

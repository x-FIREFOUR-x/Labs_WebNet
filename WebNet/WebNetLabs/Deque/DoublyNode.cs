﻿namespace Deque
{
    public class DoublyNode<T>
    {
        public DoublyNode(T value)
        {
            Value = value;
            Previous = null;
            Next = null;
        }

        public T Value { get; set; }
        public DoublyNode<T> Previous { get; set; }
        public DoublyNode<T> Next { get; set; }
    }
}

﻿using System;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            IntefaceTest consoleInterface = new IntefaceTest();

            Console.WriteLine("\n-----Test Add & Remove-----");
            consoleInterface.TestAddRemove();

            Console.WriteLine("\n-----Test Last & First-----");
            consoleInterface.TestLastFirst();

            Console.WriteLine("\n-----Test Clear-----");
            consoleInterface.TestClean();

            Console.WriteLine("\n-----Test Clone-----");
            consoleInterface.TestClone();

            Console.WriteLine("\n-----Test subscribe Event-----");
            consoleInterface.TestEvent();
        }
    }
}

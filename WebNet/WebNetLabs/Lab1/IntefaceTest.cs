﻿using System;
using Deque;

namespace Lab1
{
    class IntefaceTest
    {
        public void TestAddRemove()
        {
            Deque<int> deque = new Deque<int>();

            deque.AddFirst(5);
            deque.AddLast(1);
            deque.AddFirst(0);
            deque.AddLast(8);
            deque.AddLast(10);

            PrintDeque(deque);

            Console.WriteLine(deque.RemoveLast());
            Console.WriteLine(deque.RemoveFirst());

            PrintDeque(deque);
        }

        public void TestLastFirst()
        {
            Deque<double> deque = new Deque<double>();
            deque.AddFirst(11.11);
            deque.AddLast(22.15);
            deque.AddFirst(0.11);

            PrintDeque(deque);

            Console.WriteLine("First: " + deque.First);
            Console.WriteLine("Last: " + deque.Last);
        }
        
        public void TestClean()
        {
            Deque<string> deque = new Deque<string>();
            deque.AddFirst("abc");
            deque.AddLast("ABC");
            deque.AddFirst("G");

            PrintDeque(deque);
            Console.WriteLine("Size: " + deque.Count);

            deque.Clear();
            Console.WriteLine("Size: " + deque.Count);
        }

        public void TestClone()
        {
            Deque<int> deque1 = new Deque<int>();
            deque1.AddFirst(5);
            deque1.AddLast(10);
            deque1.AddFirst(1);

            Deque<int> deque2 = (Deque<int>)deque1.Clone();

            Console.Write("Deque1: ");
            PrintDeque(deque1);

            Console.Write("Deque2: ");
            PrintDeque(deque2);
        }

        public void TestEvent()
        {
            Deque<int> deque = new Deque<int>();

            deque.DequeEvent += (sender, x) => Console.WriteLine(x.Message);

            deque.AddFirst(5);
            deque.AddLast(10);
            deque.AddFirst(1);

            deque.RemoveLast();
            deque.RemoveFirst();

            deque.Clear();
        }

        private void PrintDeque<T>(Deque<T> deque)
        {
            foreach (var elem in deque)
            {
                Console.Write(elem + " ");
            }
            Console.WriteLine();
        }
    }
}

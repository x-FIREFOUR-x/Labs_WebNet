﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Deque;
using System;
using System.Collections.Generic;
using System.Text;

namespace Deque.Tests
{
    [TestClass()]
    public class DequeTests
    {
        private Deque<int> deque;

        [TestInitialize]
        public void TestInitionalize()
        {
            deque = new Deque<int>();
        }

        [TestCleanup]
        public void TestClean()
        {
            deque.Clear();
        }



        [TestMethod]
        public void Deque_ConstructorDeque_SuccessOperation()
        { 
            Assert.AreEqual(deque.Count, 0);
        }


        [TestMethod]
        public void AddFirst_AddElementInStartDeque_SuccessOperation()
        {
            deque.AddFirst(5);

            Assert.AreEqual(deque.First, 5);
        }


        [TestMethod]
        public void AddLast_AddElementInEndDeque_SuccessOperation()
        {
            deque.AddLast(10);

            Assert.AreEqual(deque.Last, 10);
        }


        [TestMethod]
        public void RemoveFirst_RemoveFirstElement_SuccessOperation()
        {
            deque.AddFirst(1);
            deque.AddLast(2);

            deque.RemoveFirst();

            Assert.AreEqual(deque.First, 2);
        }

        [TestMethod]
        public void RemoveFirst_RemoveFirstInEmptyDeque_ThrowInvalidOperationException()
        {
            Assert.ThrowsException<InvalidOperationException>(() => deque.RemoveFirst());
        }


        [TestMethod]
        public void RemoveLast_RemoveLastElement_SuccessOperation()
        {
            deque.AddFirst(1);
            deque.AddLast(2);

            deque.RemoveLast();

            Assert.AreEqual(deque.First, 1);
        }

        [TestMethod]
        public void RemoveLast_RemoveLastInEmptyDeque_ThrowInvalidOperationException()
        {
            Assert.ThrowsException<InvalidOperationException>(() => deque.RemoveLast());
        }


        [TestMethod]
        public void First_GetFirstElementInEmptyDeque_ThrowInvalidOperationException()
        {
            Assert.ThrowsException<InvalidOperationException>(() => deque.First);
        }

        [TestMethod]
        public void Last_GetLastElementInEmptyDeque_ThrowInvalidOperationException()
        {
            Assert.ThrowsException<InvalidOperationException>(() => deque.Last);
        }


        [TestMethod]
        public void Clear_ClearDeque_SuccessOperation()
        {
            deque.AddFirst(1);
            deque.AddLast(2);

            deque.Clear();

            Assert.AreEqual(deque.Count, 0);
        }


        [TestMethod]
        public void Contains_SearchExistingElementInDeque_ReturnedTrue()
        {
            deque.AddFirst(1);
            deque.AddLast(2);

            Assert.AreEqual(deque.Contains(2), true);
        }

        [TestMethod]
        public void Contains_SearchUnexistingElementInDeque_ReturnedFalse()
        {
            deque.AddFirst(1);
            deque.AddLast(2);

            Assert.AreEqual(deque.Contains(3), false);
        }

        [TestMethod]
        public void Contains_SearchElementInEmptyDeque_ReturnedFalse()
        {
            Assert.AreEqual(deque.Contains(1), false);
        }


        [TestMethod()]
        public void GetEnumerator_DequeEnumaration_SuccessOperation()
        {
            deque.AddFirst(1);
            deque.AddLast(2);
            deque.AddLast(3);

            bool correct = true;
            int i = 1;
            foreach (var elem in deque)
            {
                if (elem != i)
                    correct = false;
                i++;
            }

            Assert.AreEqual(correct, true);
        }


        [TestMethod()]
        public void Clone_CloneDeque_SuccessOperation()
        {
            deque.AddFirst(1);
            deque.AddLast(2);

            Deque<int> dequeClone = (Deque<int>)deque.Clone();

            CollectionAssert.AreEqual(dequeClone, deque);
        }


        [TestMethod()]
        public void CopyTo_CopyDequeToArray_SuccessOperatio()
        {
            Array array = new int[3];
            Array array2 = new int[3] {0, 1, 2};

            deque.AddFirst(1);
            deque.AddLast(2);
            deque.CopyTo(array, 1);

            CollectionAssert.AreEqual(array, array2);
        }

        [TestMethod()]
        public void CopyTo_CopyToNullArray_ThrowNotInitializeArray()
        {
            Array array = null;

            Assert.ThrowsException<ArgumentNullException>(() => deque.CopyTo(array, 1));
        }

        [TestMethod()]
        public void CopyTo_InvalidIndex_ThrowIndexOutOfRange()
        {
            Array array = new int[3];

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => deque.CopyTo(array, -1));
        }

        [TestMethod()]
        public void CopyTo_CopyToMultyDimensionalArray_ThrowArgumentException()
        {
            Array array = new int[3, 2];

            Assert.ThrowsException<ArgumentException>(() => deque.CopyTo(array, 1));
        }

        [TestMethod()]
        public void CopyTo_CopyToShortArray_ThrowArgumentException()
        {
            Array array = new int[3];
            deque.AddFirst(1);
            deque.AddLast(2);

            Assert.ThrowsException<ArgumentException>(() => deque.CopyTo(array, 2));
        }
    }
}
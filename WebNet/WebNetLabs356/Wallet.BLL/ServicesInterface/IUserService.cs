﻿using System.Threading.Tasks;

using Wallet.DAL.Entities;

using Wallet.BLL.DTO;

namespace Wallet.BLL.ServicesInterface
{
    public interface IUserService
    {
        Task<User> CreateUser(UserRegistrationData userData);

        Task<User> LoginUser(UserLoginData userData);
    }
}

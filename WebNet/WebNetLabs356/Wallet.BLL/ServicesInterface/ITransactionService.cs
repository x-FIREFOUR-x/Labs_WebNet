﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Wallet.DAL.Entities;

using Wallet.BLL.DTO;

namespace Wallet.BLL.ServicesInterface
{
    public interface ITransactionService
    {
        Task<Transaction> CreateTransaction(int moneyAccountId, CreateTransactionData transactionData);

        Task<List<Transaction>> GetInputTransactions(int moneyAccountId);

        Task<List<Transaction>> GetOutputTransactions(int moneyAccountId);

        Task<List<Transaction>> GetTransactions(int moneyAccountId);

        Task<List<Transaction>> GetFilterTransactions(int moneyAccountId, TransactionFilters filters);
    }
}

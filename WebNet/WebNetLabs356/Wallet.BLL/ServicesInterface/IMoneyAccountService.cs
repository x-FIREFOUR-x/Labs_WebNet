﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Wallet.DAL.Entities;

namespace Wallet.BLL.ServicesInterface
{
    public interface IMoneyAccountService
    {
        Task<MoneyAccount> CreateNewMoneyAccount(int userId);

        Task<List<MoneyAccount>> GetListMoneyAccounts(int userId);
    }
}

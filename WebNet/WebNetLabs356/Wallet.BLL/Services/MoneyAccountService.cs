﻿using System.Threading.Tasks;
using System;
using System.Collections.Generic;

using Wallet.DAL.Entities;
using Wallet.DAL.RepositoryInterface;

using Wallet.BLL.ServicesInterface;
using System.Linq;

namespace Wallet.BLL.Services
{
    public class MoneyAccountService: IMoneyAccountService
    {
        IUnitOfWork _unitOfWork;
        public MoneyAccountService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<MoneyAccount> CreateNewMoneyAccount(int userId)
        {
            if (await _unitOfWork.UserRepository.GetById(userId) == null)
            {
                throw new KeyNotFoundException($"User {userId} not found");
            }    

            string newCardNumber = "";
            bool isUnique = false;
            while (!isUnique)
            {
               newCardNumber = GenerateCardNumber();

               if (await _unitOfWork.MoneyAccoutRepository.GetByCardNumber(newCardNumber) == null)
               {
                    isUnique = true;
               }
            }

            MoneyAccount newMoneyAccount = new MoneyAccount
            {
                Balance = 10000,
                ExpirationDate = DateTime.Now.AddYears(2),
                CardNumber = newCardNumber,
                UserId = userId
            };

            await _unitOfWork.MoneyAccoutRepository.Add(newMoneyAccount);
            await _unitOfWork.SaveChangesAsync();

            return newMoneyAccount;
        }

        private string GenerateCardNumber()
        {
            Random rand = new Random();
            int number = rand.Next(0, 99999999);

            string cardNumber = number.ToString();
            int amountNeededZero = 8 - cardNumber.Length;

            cardNumber = string.Concat(Enumerable.Repeat("0", amountNeededZero)) + cardNumber;

            return cardNumber;
        }

        public async Task<List<MoneyAccount>> GetListMoneyAccounts(int userId)
        {
            if (await _unitOfWork.UserRepository.GetById(userId) == null)
            {
                throw new KeyNotFoundException($"User {userId} not found");
            }

            return await _unitOfWork.MoneyAccoutRepository.GetByUserId(userId);
        }
    }
}

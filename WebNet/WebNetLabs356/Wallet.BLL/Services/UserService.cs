﻿using System.Threading.Tasks;

using System.Security.Authentication;
using Microsoft.EntityFrameworkCore;

using Wallet.DAL.RepositoryInterface;
using Wallet.DAL.Entities;

using Wallet.BLL.DTO;
using Wallet.BLL.Exceptions;
using Wallet.BLL.ServicesInterface;


namespace Wallet.BLL.Services
{
    public class UserService: IUserService
    {
        IUnitOfWork _unitOfWork;
        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<User> CreateUser(UserRegistrationData userData)
        {
            try
            {
                User newUser = new User
                {
                    FirstName = userData.FirstName,
                    LastName = userData.LastName,
                    PhoneNumber = userData.PhoneNumber,
                    Login = userData.Login,
                    Password = userData.Password
                };

                await _unitOfWork.UserRepository.Add(newUser);
                await _unitOfWork.SaveChangesAsync();

                return newUser;
            }
            catch (DbUpdateException)
            {

                throw new AlreadyExistData("Login or PhoneNumber already used");
            }
            
        }

        public async Task<User?> LoginUser(UserLoginData userData)
        {
            User? user = await _unitOfWork.UserRepository.GetByLogin(userData.Login);

            if (user == null)
                throw new AuthenticationException("Login or password is not correct");

            if (user.Password != userData.Password)
                throw new AuthenticationException("Login or password is not correct");

            return user;
        }
    }
}

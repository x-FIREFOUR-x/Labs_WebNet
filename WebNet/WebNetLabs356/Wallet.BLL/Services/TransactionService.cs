﻿using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Wallet.DAL.Entities;
using Wallet.DAL.RepositoryInterface;

using Wallet.BLL.DTO;
using Wallet.BLL.Exceptions;
using Wallet.BLL.ServicesInterface;


namespace Wallet.BLL.Services
{
    public class TransactionService: ITransactionService
    {
        IUnitOfWork _unitOfWork;
        public TransactionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Transaction> CreateTransaction(int moneyAccountId, CreateTransactionData transactionData)
        {
            MoneyAccount? moneyAccount =
                await _unitOfWork.MoneyAccoutRepository.GetById(moneyAccountId);

            if (moneyAccount == null)
                throw new KeyNotFoundException($"MoneyAccount {moneyAccountId} not found");

            if (transactionData.Suma > moneyAccount.Balance)
                throw new InsufficientFundsException("Insufficient funds");

            if (moneyAccount.ExpirationDate < DateTime.Now)
                throw new ArgumentException("This card has expired ");

            if (moneyAccount.CardNumber == transactionData.RecipientCardNumber)
                throw new ArgumentException("Unable to forward money with card to self card");

            MoneyAccount recipientMoneyAccount = 
                await _unitOfWork.MoneyAccoutRepository.GetByCardNumber(transactionData.RecipientCardNumber) 
                ?? throw new ArgumentException("Unexist cardNumber");


            Transaction newTransaction = new Transaction
            {
                Suma = transactionData.Suma,
                Type = transactionData.Type,
                TimeImplementation = DateTime.Now,
                RecipientMoneyAccountId = recipientMoneyAccount.Id,
                SenderMoneyAccountId = moneyAccount.Id
            };

            recipientMoneyAccount.Balance += newTransaction.Suma;
            moneyAccount.Balance -= newTransaction.Suma;

            await _unitOfWork.TransactionRepository.Add(newTransaction);
            _unitOfWork.MoneyAccoutRepository.Update(recipientMoneyAccount);
            _unitOfWork.MoneyAccoutRepository.Update(moneyAccount);
            await _unitOfWork.SaveChangesAsync();

            return newTransaction;
        }

        public async Task<List<Transaction>> GetInputTransactions(int moneyAccountId)
        {
            if (await _unitOfWork.MoneyAccoutRepository.GetById(moneyAccountId) == null)
                throw new KeyNotFoundException($"MoneyAccount {moneyAccountId} not found");

            return await _unitOfWork.TransactionRepository.GetByRecipientMoneyAccountId(moneyAccountId);
        }

        public async Task<List<Transaction>> GetOutputTransactions(int moneyAccountId)
        {
            if (await _unitOfWork.MoneyAccoutRepository.GetById(moneyAccountId) == null)
                throw new KeyNotFoundException($"MoneyAccount {moneyAccountId} not found");

            List<Transaction> transactions = 
                await _unitOfWork.TransactionRepository.GetBySenderMoneyAccountId(moneyAccountId);

            return transactions.Select(tr => { tr.Suma *= -1; return tr; }).ToList();
        }

        public async Task<List<Transaction>> GetTransactions(int moneyAccountId)
        {
            if (await _unitOfWork.MoneyAccoutRepository.GetById(moneyAccountId) == null)
                throw new KeyNotFoundException($"MoneyAccount {moneyAccountId} not found");

            List<Transaction> transactions = await GetOutputTransactions(moneyAccountId);
            transactions.AddRange(await GetInputTransactions(moneyAccountId));

            return transactions.OrderBy(x => x.TimeImplementation).ToList();
        }

        private Expression<Func<Transaction, bool>> CombineExpresionsAnd(
            Expression<Func<Transaction, bool>> expr1,
            Expression<Func<Transaction, bool>> expr2)
        {
            var param = Expression.Parameter(typeof(Transaction), "x");
            var body = Expression.AndAlso(
                    Expression.Invoke(expr1, param),
                    Expression.Invoke(expr2, param));
            return Expression.Lambda<Func<Transaction, bool>>(body, param);
        }

        private Expression<Func<Transaction, bool>> GetExpressionWithFilters(TransactionFilters filters)
        {
            Expression<Func<Transaction, bool>> filterExpression = x => true;
            if (filters.Type != null)
            {
                Expression<Func<Transaction, bool>> nextFilter = x => (x.Type == filters.Type);
                filterExpression = CombineExpresionsAnd(filterExpression, nextFilter);
            }

            if (filters.FromDate != default(DateTime))
            {
                Expression<Func<Transaction, bool>> nextFilter = x => (x.TimeImplementation >= filters.FromDate);
                filterExpression = CombineExpresionsAnd(filterExpression, nextFilter);
            }

            if (filters.ToDate != default(DateTime))
            {
                Expression<Func<Transaction, bool>> nextFilter = x => (x.TimeImplementation <= filters.ToDate);
                filterExpression = CombineExpresionsAnd(filterExpression, nextFilter);
            }

            return filterExpression;
        }
        
        public async Task<List<Transaction>> GetFilterTransactions(int moneyAccountId, TransactionFilters filters)
        {
            if (await _unitOfWork.MoneyAccoutRepository.GetById(moneyAccountId) == null)
                throw new KeyNotFoundException($"MoneyAccount {moneyAccountId} not found");

            Expression<Func<Transaction, bool>> filterExpression = GetExpressionWithFilters(filters);

            List<Transaction>? inputTransactions = null;
            if (filters.Direction == DirectionTransaction.InputTransaction ||
                filters.Direction == DirectionTransaction.AllTransaction)
            {
                inputTransactions = await _unitOfWork.TransactionRepository
                    .GetByResipientMoneyAccountWhereId(moneyAccountId, filterExpression);
            }

            List<Transaction>? outputTransactions = null;
            if (filters.Direction == DirectionTransaction.OutputTransaction ||
                filters.Direction == DirectionTransaction.AllTransaction)
            {
                outputTransactions = await _unitOfWork.TransactionRepository
                    .GetBySenderMoneyAccountWhereId(moneyAccountId, filterExpression);

                outputTransactions = outputTransactions.Select(tr => { tr.Suma *= -1; return tr; }).ToList();
            }

            List<Transaction>? resultTransactions = filters.Direction switch
            {
                DirectionTransaction.InputTransaction => inputTransactions,
                DirectionTransaction.OutputTransaction => outputTransactions,
                _ => inputTransactions.Concat(outputTransactions).OrderBy(x => x.TimeImplementation).ToList()
            };

            return resultTransactions;
        }
    }
}

﻿using System;

namespace Wallet.BLL.Exceptions
{
    public class AlreadyExistData: Exception
    {
        public AlreadyExistData()
        {
        }

        public AlreadyExistData(string message)
            : base(message)
        {
        }

        public AlreadyExistData(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}

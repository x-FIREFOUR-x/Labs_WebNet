﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wallet.BLL.Validators
{
    public static class ValidationBDField
    {
        public static IReadOnlyDictionary<string, string> RegularExpressions = new Dictionary<string, string>
        {
            { "Email", @"^([a-zA-z0-9]+([._\-][a-zA-z0-9]+)?)+@([a-zA-z0-9]+([.\-][a-zA-Z0-9]+)?)+\.[a-zA-Z]{2,4}$" },
            { "Name", @"^[\w\d]+[\w\d\- іІїЇ]+$" },
            { "Login", @"^[a-zA-Z\d-_]+$" },
            { "Password", @"^[a-zA-Z\d-]+$" },
            { "Description", @"^[.,іІїЇa-zA-Z\dа-яА-Я-\s]*$" },
            { "PhoneCode", @"^\d{1,3}$"},
            { "PhoneNumber", @"^\d{9,12}$" },
            { "CardNumber", @"^\d{8}$" },
            { "Date", "^([0]?[0-9]|[12][0-9]|[3][01])[.]([0]?[1-9]|[1][0-2])[.]([0-9]{4}|[0-9]{2})$" },
            { "Money", @"^\d+(?:\.\d{1,2})?$" }
        };

        public static bool IsValidFirstName(this string value)
        {
            var pattern = RegularExpressions["Name"];
            return Regex.IsMatch(value, pattern);
        }

        public static bool IsValidLastName(this string value)
        {
            var pattern = RegularExpressions["Name"];
            return Regex.IsMatch(value, pattern);
        }

        public static bool IsValidPhoneNumber(this string value)
        {
            var pattern = RegularExpressions["PhoneNumber"];
            return Regex.IsMatch(value, pattern);
        }

        public static bool IsValidLogin(this string value)
        {
            var pattern = RegularExpressions["Login"];
            return Regex.IsMatch(value, pattern);
        }

        public static bool IsValidPassword(this string value)
        {
            var pattern = RegularExpressions["Password"];
            return Regex.IsMatch(value, pattern);
        }

        public static bool IsValidDate(this string value)
        {
            var pattern = RegularExpressions["Date"];
            return Regex.IsMatch(value, pattern);
        }

        public static bool IsValidMoney(this string value)
        {
            var pattern = RegularExpressions["Money"];
            return Regex.IsMatch(value, pattern);
        }

        public static bool IsValidDescription(this string value)
        {
            var pattern = RegularExpressions["Description"];
            return Regex.IsMatch(value, pattern);
        }

        public static bool IsValidCardNumber(this string value)
        {
            var pattern = RegularExpressions["CardNumber"];
            return Regex.IsMatch(value, pattern);
        }
    }
}

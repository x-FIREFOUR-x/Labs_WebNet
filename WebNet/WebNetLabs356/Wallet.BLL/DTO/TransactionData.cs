﻿using System;

namespace Wallet.BLL.DTO
{
    public class TransactionData
    {
        public int Id { get; set; }
        public double Suma { get; set; }
        public string? Type { get; set; }
        public DateTime TimeImplementation { get; set; }
        public int RecipientMoneyAccountId { get; set; }
        public int SenderMoneyAccountId { get; set; }
    }
}

﻿using System;

namespace Wallet.BLL.DTO
{
    public class MoneyAccountData
    {
        public int Id { get; set; }
        public double Balance { get; set; }
        public string CardNumber { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int UserId { get; set; }
    }
}

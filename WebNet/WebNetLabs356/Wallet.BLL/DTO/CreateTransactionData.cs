﻿namespace Wallet.BLL.DTO
{
    public class CreateTransactionData
    {
        public double Suma { get; set; }
        public string Type { get; set; }
        public string RecipientCardNumber { get; set; }
    }
}

﻿using System;

namespace Wallet.BLL.DTO
{
    public enum DirectionTransaction
    {
        AllTransaction,
        InputTransaction,
        OutputTransaction
    }

    public class TransactionFilters
    {
        public DirectionTransaction Direction { get; set; }
        public string? Type { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public TransactionFilters()
        {
            Direction = DirectionTransaction.AllTransaction;
            Type = null;
            FromDate = default(DateTime);
            ToDate = default(DateTime);
        }

    }
}

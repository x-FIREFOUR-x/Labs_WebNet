﻿using Wallet.Console.View;

using Autofac;
using System.Threading.Tasks;

using Wallet.BLL.Services;
using Wallet.BLL.ServicesInterface;

using Wallet.DAL.Repository;
using Wallet.DAL.RepositoryInterface;
using Wallet.DAL.Context;
using Wallet.DAL.Entities;

namespace Wallet.Console
{
    class Program
    {
        private static IContainer Container { get; set; }
        static async Task Main(string[] args)
        {   
            var builder = new ContainerBuilder();

            builder.RegisterType<RepositoryUser>().As<IRepositoryUser>();
            builder.RegisterType<RepositoryMoneyAccount>().As<IRepositoryMoneyAccount>();
            builder.RegisterType<RepositoryTransaction>().As<IRepositoryTransaction>();
            

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<MoneyAccountService>().As<IMoneyAccountService>();
            builder.RegisterType<TransactionService>().As<ITransactionService>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

            builder
                .RegisterType<WalletDbContext>()
                .WithParameter("options", DbContextOptionsFactory.Get())
                .InstancePerLifetimeScope();

            Container = builder.Build();

            while(true)
            {
                EnterView enterView = new EnterView(Container);
                User user = await enterView.Enter();

                if (user != null)
                {
                    UserMenuView userMenuView = new UserMenuView(Container, user);
                    await userMenuView.UserMenuProcess();
                }
            }  
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Autofac;

using Wallet.DAL.Entities;

using Wallet.BLL.ServicesInterface;

namespace Wallet.Console.View
{
    class UserMenuView
    {
        IContainer _container;
        User logedUser;

        public UserMenuView(IContainer container, User user)
        {
            _container = container;
            logedUser = user;
        }

        public async Task UserMenuProcess()
        {
            System.Console.Write("Input 1- Look profile, 2- Look money accounts, 3 - Create money account, Other - Exit: ");
            string operation = System.Console.ReadLine();

            if (operation == "1")
            {
                System.Console.WriteLine("Name: {0}", logedUser.FirstName);
                System.Console.WriteLine("Surname: {0}", logedUser.LastName);
                System.Console.WriteLine("PhoneNumber: {0}", logedUser.PhoneNumber);
                await UserMenuProcess();
            }
            else if(operation == "2")
            {
                List<MoneyAccount> moneyAccounts = await ShowMoneyAccounts();
                MoneyAccount moneyAccount = await ChooseCard(moneyAccounts);

                if (moneyAccount == null)
                {
                    await UserMenuProcess();
                }
                else
                {
                    TransactionMenuView transactionView = new TransactionMenuView(_container, moneyAccount);
                    await transactionView.TransactionMenu();
                    await UserMenuProcess();
                }
            }
            else if (operation == "3")
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var moneyAccountService = scope.Resolve<IMoneyAccountService>();
                    await moneyAccountService.CreateNewMoneyAccount(logedUser.Id);
                }
                await UserMenuProcess();
            }

        }

        public async Task<List<MoneyAccount>> ShowMoneyAccounts()
        {
            
            List<MoneyAccount> moneyAccounts = null;

            using (var scope = _container.BeginLifetimeScope())
            {
                var moneyAccountService = scope.Resolve<IMoneyAccountService>();
                moneyAccounts = await moneyAccountService.GetListMoneyAccounts(logedUser.Id);
            }

            System.Console.WriteLine();
            int number = 1;
            foreach (var moneyAccount in moneyAccounts)
            {
                System.Console.WriteLine("=============={0}==============", number);
                System.Console.WriteLine("Cardnumber: {0}", moneyAccount.CardNumber);
                System.Console.WriteLine("Date: {0}", moneyAccount.ExpirationDate);
                System.Console.WriteLine("Balance: {0}", moneyAccount.Balance);
                number++;
            }
            if (number == 1)
            {
                System.Console.WriteLine("Unexist money account");
            }
            System.Console.WriteLine();

            return moneyAccounts;
        }

        public async Task<MoneyAccount?> ChooseCard(List<MoneyAccount> moneyAccounts)
        {
            System.Console.Write("Choose money account (input number money account or 0 - back): ");
            string input = System.Console.ReadLine();

            if (input == "0")
                return null;

            int index = int.Parse(input);
            MoneyAccount moneyAccount = null;

            using (var scope = _container.BeginLifetimeScope())
            {
                try
                {
                    var moneyAccountService = scope.Resolve<IMoneyAccountService>();
                    moneyAccount = moneyAccounts[index - 1];
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    System.Console.WriteLine( " !Fail. Uncorrect number");
                    moneyAccount = await ChooseCard(moneyAccounts);
                }
            }
            

            return moneyAccount;
        }

       

    }
}

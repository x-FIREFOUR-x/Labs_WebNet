﻿using System;
using System.Threading.Tasks;
using System.Security.Authentication;

using Autofac;

using Wallet.BLL.DTO;
using Wallet.BLL.Validators;
using Wallet.BLL.Exceptions;
using Wallet.BLL.ServicesInterface;

using Wallet.DAL.Entities;

namespace Wallet.Console.View
{
    class EnterView
    {
        IContainer _container;

        public EnterView(IContainer container)
        {
            _container = container;
        }
        public async Task<User?> Enter()
        {
            System.Console.Write("1- Login, 2-Registration: ");
            string input = System.Console.ReadLine();
            User user= null;

            if (input == "1")
            {
                user = await Login();
            }
            else if (input == "2")
            {
                await Registration();
            }
            else
            {
                System.Console.WriteLine("Uncorrect operation");
                await Enter();
            }

            return user;
        }

        private async Task<User> Login()
        {
            string login = InputField("Login: ", ValidationBDField.IsValidLogin);
            string password = InputField("Password: ", ValidationBDField.IsValidPassword);

            UserLoginData userData = new UserLoginData
            {
                Login = login,
                Password = password
            };

            using (var scope = _container.BeginLifetimeScope())
            {
                var userService = scope.Resolve<IUserService>();

                try
                {
                    User loginedUser = await userService.LoginUser(userData);
                    return loginedUser;
                }
                catch (AuthenticationException ex)
                {
                    System.Console.WriteLine( " !Fail. " + ex.Message);
                    User loginedUser = await Login();
                    return loginedUser;
                }
            }
        }
        private async Task Registration()
        {
            string firstName = InputField("FirstName: ", ValidationBDField.IsValidFirstName);
            string lastName = InputField("LastName: ", ValidationBDField.IsValidLastName);
            string phoneNumbere = InputField("PhoneNumber: ", ValidationBDField.IsValidPhoneNumber);
            string login = InputField("Login: ", ValidationBDField.IsValidLogin);
            string password = InputField("Password: ", ValidationBDField.IsValidPassword);

            UserRegistrationData UserData = new UserRegistrationData
            {
                FirstName = firstName,
                LastName = lastName,
                PhoneNumber = phoneNumbere,
                Login = login,
                Password = password
            };

            using (var scope = _container.BeginLifetimeScope())
            {
                var userService = scope.Resolve<IUserService>();

                try
                {
                    await userService.CreateUser(UserData);
                }
                catch (AlreadyExistData ex)
                {
                    System.Console.WriteLine( " !Fail. " + ex.Message + ", try again");
                }
                
            }

        }

        private string InputField(string outputMessage, Func<string, bool> Validator)
        {
            bool isCorrectField = false;
            string inputData = "";
            while (!isCorrectField)
            {
                System.Console.Write(outputMessage);
                inputData = System.Console.ReadLine();
                isCorrectField = Validator(inputData);
            }

            return inputData;
        }
    }
}

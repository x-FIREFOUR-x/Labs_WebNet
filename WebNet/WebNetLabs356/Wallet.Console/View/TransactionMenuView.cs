﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Autofac;

using Wallet.BLL.DTO;
using Wallet.BLL.Validators;
using Wallet.BLL.Exceptions;
using Wallet.BLL.ServicesInterface;

using Wallet.DAL.Entities;

namespace Wallet.Console.View
{
    public class TransactionMenuView
    {
        IContainer _container;
        MoneyAccount _moneyAccount;

        public TransactionMenuView(IContainer container, MoneyAccount moneyAccount)
        {
            _container = container;
            _moneyAccount = moneyAccount;
        }

        public async Task TransactionMenu()
        {
            System.Console.Write("1- Send money, 2 - Check Transaction, other - back: ");
            string input = System.Console.ReadLine();

            if (input == "1")
            {
                await SendMoney();
                await TransactionMenu();
            }
            else if (input == "2")
            {
                await ShowTransaction();
                await TransactionMenu();
            }
        }

        public async Task SendMoney()
        {
            string cardnumber = InputField("Recipient cardnumber: ", ValidationBDField.IsValidCardNumber);
            string sumaStr = InputField("Suma: ", ValidationBDField.IsValidMoney);
            double suma = double.Parse(sumaStr);
            string type = InputField("Type transaction: ", ValidationBDField.IsValidDescription);

            CreateTransactionData newTransaction = new CreateTransactionData
            {
                Suma = suma,
                RecipientCardNumber = cardnumber,
                Type = type,
            };

            try
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var transactionService = scope.Resolve<ITransactionService>();
                    await transactionService.CreateTransaction(_moneyAccount.Id, newTransaction);
                }
            }
            catch (InsufficientFundsException ex)
            {
                System.Console.WriteLine(" !Fail Transaction." + ex.Message);
                await SendMoney();
            }
            catch (ArgumentException ex)
            {
                System.Console.WriteLine( " !Fail Transaction." + ex.Message);
                await SendMoney();
            }
        }


        public async Task ShowTransaction()
        {

            DirectionTransaction dirTransaction = 
                InputDirTransaction("Direction transactions. Input 1-input , 2-output, 3-all transactions: ");

            string type = InputField("Input type transaction: ", ValidationBDField.IsValidDescription);
            string strDateFrom = InputField("Input from which date: ", ValidationBDField.IsValidDate);
            string strDateTo = InputField("Input to which date: ", ValidationBDField.IsValidDate);


            TransactionFilters filters = new TransactionFilters { Direction = dirTransaction};

            filters.Type = null;
            if (type != "")
                filters.Type = type;

            filters.FromDate = default(DateTime);
            if (strDateFrom != "")
                filters.FromDate = Parse(strDateFrom);

            filters.ToDate = default(DateTime);
            if (strDateTo != "")
                filters.ToDate = Parse(strDateTo);


            using (var scope = _container.BeginLifetimeScope())
            {
                var transactionService = scope.Resolve<ITransactionService>();
                List<Transaction> transactions =  await transactionService.GetFilterTransactions(_moneyAccount.Id, filters);

                foreach (var transaction in transactions)
                {
                    System.Console.WriteLine("----------------------");
                    System.Console.WriteLine("\t Sender: " + transaction.SenderMoneyAccount.CardNumber);
                    System.Console.WriteLine("\t Resipient: " + transaction.RecipientMoneyAccount.CardNumber);
                    System.Console.WriteLine("\t Suma: " + transaction.Suma);
                    System.Console.WriteLine("\t Date: " + transaction.TimeImplementation);
                    System.Console.WriteLine("\t Type: " + transaction.Type);
                }

                System.Console.WriteLine();
                if (transactions.Count == 0)
                {
                    System.Console.WriteLine("\t Unexist transactions");
                }
            }

        }

        private DirectionTransaction InputDirTransaction(string outputMessage)
        {
            DirectionTransaction dirTransaction = DirectionTransaction.InputTransaction;
            bool isCorrectField = false;
            string inputData = "";
            
            while (!isCorrectField)
            {
                System.Console.Write(outputMessage);
                inputData = System.Console.ReadLine();

                if (inputData == "1")
                {
                    dirTransaction = DirectionTransaction.InputTransaction;
                    isCorrectField = true;
                }
                else if (inputData == "2")
                {
                    dirTransaction = DirectionTransaction.OutputTransaction;
                    isCorrectField = true;
                }
                else if (inputData == "3")
                {
                    dirTransaction = DirectionTransaction.AllTransaction;
                    isCorrectField = true;
                }
                else
                {
                    System.Console.WriteLine(" !Fail. Uncorrect operation");
                }
            }

            return dirTransaction;
        }

        private string InputField(string outputMessage, Func<string, bool> Validator)
        {
            bool isCorrectField = false;
            string inputData = "";
            while (!isCorrectField)
            {
                System.Console.Write(outputMessage);
                inputData = System.Console.ReadLine();

                if (inputData == "")
                    isCorrectField = true;
                else
                    isCorrectField = Validator(inputData);
            }

            return inputData;
        }

        private DateTime Parse(string date)
        {
            var strs=  date.Split(".");

            return new DateTime(int.Parse(strs[2]), int.Parse(strs[1]), int.Parse(strs[0]));
        }
    }
}

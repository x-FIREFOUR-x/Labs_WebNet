using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Authentication;
using System.Threading.Tasks;

using Wallet.BLL.DTO;
using Wallet.BLL.Exceptions;
using Wallet.BLL.Services;
using Wallet.BLL.Test.MockBD;

using Wallet.DAL.Entities;

namespace Wallet.BLL.Test
{
    [TestClass]
    public class UserServiceTests
    {
        private static UserService userService;
        private static MockUnitOfWork unitOfWork;
        [TestInitialize]
        public void TestInitionalize()
        {
            MockWalletDbContext dataBase = new MockWalletDbContext();
            unitOfWork = new MockUnitOfWork(dataBase);
            userService = new UserService(unitOfWork);
        }

        [TestMethod]
        public async Task UserService_CreateUser_SuccessOperation()
        {
            string newlogin = "CorrectUser";
            UserRegistrationData UserData = new UserRegistrationData
            {
                FirstName = "����",
                LastName = "�����",
                PhoneNumber = "380676987654",
                Login = newlogin,
                Password = "Password"
            };

            await userService.CreateUser(UserData);
            User newUser = await unitOfWork.UserRepository.GetByLogin(newlogin);

            Assert.AreEqual(newUser.Login, newlogin);
        }

        
        [TestMethod]
        public async Task UserService_CreateUser_ThrowUsedLogin()
        {
            UserRegistrationData UserData = new UserRegistrationData
            {
                FirstName = "����",
                LastName = "�����",
                PhoneNumber = "380676987654",
                Login = "User1",
                Password = "Password"
            };

            await Assert.ThrowsExceptionAsync<AlreadyExistData>(
                () => userService.CreateUser(UserData));
        }

        [TestMethod]
        public async Task UserService_CreateUser_ThrowUsedPhoneNumber()
        {
            UserRegistrationData UserData = new UserRegistrationData
            {
                FirstName = "����",
                LastName = "�����",
                PhoneNumber = "380670123456",
                Login = "CorrectUser",
                Password = "Password"
            };

            await Assert.ThrowsExceptionAsync<AlreadyExistData>(
                () => userService.CreateUser(UserData));
        }

        [TestMethod]
        public async Task UserService_LoginUser_SuccessOperation()
        {
            UserLoginData userData = new UserLoginData 
            { 
                Login = "User1", 
                Password = "Password" 
            };

            User loginedUser = await userService.LoginUser(userData);

            Assert.AreNotEqual(loginedUser, null);
        }

        [TestMethod]
        public async Task UserService_LoginUser_UnexistLogin()
        {
            UserLoginData userData = new UserLoginData
            {
                Login = "UnexistLogin",
                Password = "Password"
            };

            await Assert.ThrowsExceptionAsync<AuthenticationException>(
                () => userService.LoginUser(userData));
        }

        [TestMethod]
        public async Task UserService_LoginUser_UncorrectPassword()
        {
            UserLoginData userData = new UserLoginData
            {
                Login = "User1",
                Password = "UncorrectPassword"
            };

            await Assert.ThrowsExceptionAsync<AuthenticationException>(
                () => userService.LoginUser(userData));
        }
    }
}

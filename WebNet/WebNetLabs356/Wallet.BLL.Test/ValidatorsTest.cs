﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

using Wallet.BLL.Validators;

namespace Wallet.BLL.Test
{
    [TestClass]
    public class ValidatorsTest
    {
        [TestMethod]
        public void ValidationBDField_Name_SuccessOperation()
        {
            string name = "Кім Ша 2";
            bool isCorrect = ValidationBDField.IsValidFirstName(name);

            Assert.IsTrue(isCorrect);
        }




        [TestMethod]
        public void ValidationBDField_PhoneNumber_SuccessOperation()
        {
            string phoneNumber = "380676123456";
            bool isCorrect = ValidationBDField.IsValidPhoneNumber(phoneNumber);

            Assert.IsTrue(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_PhoneNumber_FailNotDigit()
        {
            string phoneNumber = "380676123f56";
            bool isCorrect = ValidationBDField.IsValidPhoneNumber(phoneNumber);

            Assert.IsFalse(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_PhoneNumber_FailLong()
        {
            string phoneNumber = "3806761231562";
            bool isCorrect = ValidationBDField.IsValidPhoneNumber(phoneNumber);

            Assert.IsFalse(isCorrect);
        }



        [TestMethod]
        public void ValidationBDField_Login_SuccessOperation()
        {
            string login = "Login12";
            bool isCorrect = ValidationBDField.IsValidLogin(login);

            Assert.IsTrue(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Login_FailUncorrectSymbols()
        {
            string login = "Loginб";
            bool isCorrect = ValidationBDField.IsValidLogin(login);

            Assert.IsFalse(isCorrect);
        }




        [TestMethod]
        public void ValidationBDField_Password_SuccessOperation()
        {
            string password = "Password";
            bool isCorrect = ValidationBDField.IsValidPassword(password);

            Assert.IsTrue(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Password_FailSpace()
        {
            string password = "Password pas";
            bool isCorrect = ValidationBDField.IsValidPassword(password);

            Assert.IsFalse(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Password_FailUncorrectSymbols()
        {
            string password = "Passwordб";
            bool isCorrect = ValidationBDField.IsValidPassword(password);

            Assert.IsFalse(isCorrect);
        }
        


        [TestMethod]
        public void ValidationBDField_Date_SuccessOperation()
        {
            string date = "25.10.2022";
            bool isCorrect = ValidationBDField.IsValidDate(date);

            Assert.IsTrue(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Date_FailSeperate()
        {
            string date = "25,10,2022";
            bool isCorrect = ValidationBDField.IsValidDate(date);

            Assert.IsFalse(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Date_FailUncorrectSymbols()
        {
            string date = "2a.10.2022";
            bool isCorrect = ValidationBDField.IsValidDate(date);

            Assert.IsFalse(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Date_FailUncorrecMonth()
        {
            string date = "21.13.2022";
            bool isCorrect = ValidationBDField.IsValidDate(date);

            Assert.IsFalse(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Date_FailUncorrecDay()
        {
            string date = "32.12.2022";
            bool isCorrect = ValidationBDField.IsValidDate(date);

            Assert.IsFalse(isCorrect);
        }


        [TestMethod]
        public void ValidationBDField_Money_SuccessOperationDecimal()
        {
            string money = "100.50";
            bool isCorrect = ValidationBDField.IsValidMoney(money);

            Assert.IsTrue(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Money_SuccessOperationInt()
        {
            string money = "100";
            bool isCorrect = ValidationBDField.IsValidMoney(money);

            Assert.IsTrue(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Money_FailNegative()
        {
            string money = "-100";
            bool isCorrect = ValidationBDField.IsValidMoney(money);

            Assert.IsFalse(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_Money_FailUncorrectSymbol()
        {
            string money = "100f";
            bool isCorrect = ValidationBDField.IsValidMoney(money);

            Assert.IsFalse(isCorrect);
        }



        [TestMethod]
        public void ValidationBDField_Description_SuccessOperation()
        {
            string description = "Привіт, це я11. Hello";
            bool isCorrect = ValidationBDField.IsValidDescription(description);

            Assert.IsTrue(isCorrect);
        }



        [TestMethod]
        public void ValidationBDField_CardNumber_SuccessOperation()
        {
            string cardNumber = "12345678";
            bool isCorrect = ValidationBDField.IsValidCardNumber(cardNumber);

            Assert.IsTrue(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_CardNumber_FailLong()
        {
            string cardNumber = "123456789";
            bool isCorrect = ValidationBDField.IsValidCardNumber(cardNumber);

            Assert.IsFalse(isCorrect);
        }

        [TestMethod]
        public void ValidationBDField_CardNumber_FailNotDigit()
        {
            string cardNumber = "123456a8";
            bool isCorrect = ValidationBDField.IsValidCardNumber(cardNumber);

            Assert.IsFalse(isCorrect);
        }
    }
}

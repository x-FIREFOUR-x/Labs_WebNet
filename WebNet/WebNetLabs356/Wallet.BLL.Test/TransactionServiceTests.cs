﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Wallet.BLL.DTO;
using Wallet.BLL.Exceptions;
using Wallet.BLL.Services;
using Wallet.BLL.Test.MockBD;

using Wallet.DAL.Entities;

namespace Wallet.BLL.Test
{

    [TestClass]
    public class TransactionServiceTests
    {
        private static TransactionService transactionService;
        private static MockUnitOfWork unitOfWork;
        private static MockWalletDbContext dataBase;
        [TestInitialize]
        public void TestInitionalize()
        {
            dataBase = new MockWalletDbContext();
            unitOfWork = new MockUnitOfWork(dataBase);
            transactionService = new TransactionService(unitOfWork);
        }

        [TestMethod]
        public async Task TransactionService_CreateTransaction_SuccessOperation()
        {
            string typeTransaction = "TestType";
            int idSenderMoneyAccount = 1;
            int idRecipientMoneyAccount = 2;
            MoneyAccount senderMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idSenderMoneyAccount).FirstOrDefault();
            MoneyAccount recipientMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idRecipientMoneyAccount).FirstOrDefault();
            CreateTransactionData transactionData = new CreateTransactionData
            {
                Suma = 500,
                RecipientCardNumber = recipientMoneyAccount.CardNumber,
                Type = typeTransaction
            };

            await transactionService.CreateTransaction(senderMoneyAccount.Id, transactionData);

            Transaction createdTransaction = dataBase.Transactions
                .Where(t => (
                              t.RecipientMoneyAccountId == idRecipientMoneyAccount &&
                              t.SenderMoneyAccountId == idSenderMoneyAccount &&
                              t.Type == typeTransaction)
                             ).FirstOrDefault();
            Assert.IsNotNull(createdTransaction);
        }

        [TestMethod]
        public async Task TransactionService_CreateTransaction_ThrowInsufficientFundsException()
        {
            string typeTransaction = "TestType";
            int idSenderMoneyAccount = 1;
            int idRecipientMoneyAccount = 2;
            MoneyAccount senderMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idSenderMoneyAccount).FirstOrDefault();
            MoneyAccount recipientMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idRecipientMoneyAccount).FirstOrDefault();
            CreateTransactionData transactionData = new CreateTransactionData
            {
                Suma = 100000,
                RecipientCardNumber = recipientMoneyAccount.CardNumber,
                Type = typeTransaction
            };

            await Assert.ThrowsExceptionAsync<InsufficientFundsException>(
                () => transactionService.CreateTransaction(senderMoneyAccount.Id, transactionData));
        }

        [TestMethod]
        public async Task TransactionService_CreateTransaction_ThrowExpiredCard()
        {
            string typeTransaction = "TestType";
            int idSenderMoneyAccount = 3;
            int idRecipientMoneyAccount = 2;
            MoneyAccount senderMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idSenderMoneyAccount).FirstOrDefault();
            MoneyAccount recipientMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idRecipientMoneyAccount).FirstOrDefault();
            CreateTransactionData transactionData = new CreateTransactionData
            {
                Suma = 1000,
                RecipientCardNumber = recipientMoneyAccount.CardNumber,
                Type = typeTransaction
            };

            await Assert.ThrowsExceptionAsync<ArgumentException>(
                () => transactionService.CreateTransaction(senderMoneyAccount.Id, transactionData));
        }

        [TestMethod]
        public async Task TransactionService_CreateTransaction_ThrowSendCardEquelRecipientCard()
        {
            string typeTransaction = "TestType";
            int idSenderMoneyAccount = 1;
            int idRecipientMoneyAccount = 1;
            MoneyAccount senderMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idSenderMoneyAccount).FirstOrDefault();
            MoneyAccount recipientMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idRecipientMoneyAccount).FirstOrDefault();
            CreateTransactionData transactionData = new CreateTransactionData
            {
                Suma = 1000,
                RecipientCardNumber = recipientMoneyAccount.CardNumber,
                Type = typeTransaction
            };

            await Assert.ThrowsExceptionAsync<ArgumentException>(
                () => transactionService.CreateTransaction(senderMoneyAccount.Id, transactionData));
        }

        [TestMethod]
        public async Task TransactionService_CreateTransaction_ThrowUnexistCard()
        {
            string unexistRecipientCardNumber = "0000";
            string typeTransaction = "TestType";
            int idSenderMoneyAccount = 1;
            
            MoneyAccount senderMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idSenderMoneyAccount).FirstOrDefault();
            CreateTransactionData transactionData = new CreateTransactionData
            {
                Suma = 1000,
                RecipientCardNumber = unexistRecipientCardNumber,
                Type = typeTransaction
            };

            await Assert.ThrowsExceptionAsync<ArgumentException>(
                () => transactionService.CreateTransaction(senderMoneyAccount.Id, transactionData));
        }

        [TestMethod]
        public async Task TransactionService_CreateTransaction_ThrowKeyNotFoundException()
        {
            int unexistMoneyAccountId = 5;

            int idRecipientMoneyAccount = 2;
            MoneyAccount recipientMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == idRecipientMoneyAccount).FirstOrDefault();
            CreateTransactionData transactionData = new CreateTransactionData
            {
                Suma = 500,
                RecipientCardNumber = recipientMoneyAccount.CardNumber,
                Type = ""
            };

            await Assert.ThrowsExceptionAsync<KeyNotFoundException>(
                () => transactionService.CreateTransaction(unexistMoneyAccountId, transactionData));
        }



        [TestMethod]
        public async Task TransactionService_GetInputTransactions_SuccessOperation()
        {
            int expectedIdTransaction = 1;

            int idRecipientMoneyAccount = 3;
            
            List<Transaction> transactions = await transactionService.GetInputTransactions(idRecipientMoneyAccount);

            Assert.AreEqual(expectedIdTransaction, transactions[0].Id);
        }

        [TestMethod]
        public async Task TransactionService_GetInputTransactions_ThrowKeyNotFoundException()
        {
            int unexistMoneyAccountId = 5;

            await Assert.ThrowsExceptionAsync<KeyNotFoundException>(
                () => transactionService.GetInputTransactions(unexistMoneyAccountId));
        }



        [TestMethod]
        public async Task TransactionService_GetOutputTransactions_SuccessOperation()
        {
            int expectedIdTransaction = 3;
            int idSenderMoneyAccount = 3;

            List<Transaction> transactions = await transactionService.GetOutputTransactions(idSenderMoneyAccount);

            Assert.AreEqual(expectedIdTransaction, transactions[0].Id);
        }

        [TestMethod]
        public async Task TransactionService_GetOutputTransactions_ThrowKeyNotFoundException()
        {
            int unexistMoneyAccountId = 5;

            await Assert.ThrowsExceptionAsync<KeyNotFoundException>(
                () => transactionService.GetOutputTransactions(unexistMoneyAccountId));
        }



        [TestMethod]
        public async Task TransactionService_GetTransactions_SuccessOperation()
        {
            List<int> expectedIdTransactions = new List<int>(){1, 3};
            int idMoneyAccount = 3;

            List<Transaction> transactions = await transactionService.GetTransactions(idMoneyAccount);

            bool expectedIds = true;
            foreach (var transaction in transactions)
            {
                if (!expectedIdTransactions.Contains(transaction.Id))
                    expectedIds = false;
            }
            Assert.IsTrue(expectedIds);
        }

        [TestMethod]
        public async Task TransactionService_GetTransactions_ThrowKeyNotFoundException()
        {
            int unexistMoneyAccountId = 5;

            await Assert.ThrowsExceptionAsync<KeyNotFoundException>(
                () => transactionService.GetTransactions(unexistMoneyAccountId));
        }



        [TestMethod]
        public async Task TransactionService_GetFilterTransactions_SuccessCombineFilters()
        {
            List<int> expectedIdTransactions = new List<int>() { 3, 5 };
            int idMoneyAccount = 1;
            TransactionFilters filters = new TransactionFilters 
            { 
                Direction = DirectionTransaction.AllTransaction,
                Type = "Travel",
                FromDate = new DateTime(2021, 12, 2),
                ToDate = new DateTime(2022, 11, 14)
            };

            List<Transaction> transactions = await transactionService.GetFilterTransactions(idMoneyAccount, filters);

            bool expectedIds = true;
            foreach (var transaction in transactions)
            {
                if (!expectedIdTransactions.Contains(transaction.Id))
                    expectedIds = false;
            }
            Assert.IsTrue(expectedIds);
        }

        [TestMethod]
        public async Task TransactionService_GetFilterTransactions_SuccessFilterDiractionInput()
        {
            int expectedIdTransaction = 1;

            int idMoneyAccount = 3;
            TransactionFilters filters = new TransactionFilters
            {
                Direction = DirectionTransaction.InputTransaction
            };


            List<Transaction> transactions = await transactionService.GetFilterTransactions(idMoneyAccount, filters);

            Assert.AreEqual(expectedIdTransaction, transactions[0].Id);
        }

        [TestMethod]
        public async Task TransactionService_GetFilterTransactions_SuccessFilterDiractionOutput()
        {
            int expectedIdTransaction = 3;
            int idMoneyAccount = 3;
            
            TransactionFilters filters = new TransactionFilters
            {
                Direction = DirectionTransaction.OutputTransaction
            };

            List<Transaction> transactions = await transactionService.GetFilterTransactions(idMoneyAccount, filters);

            Assert.AreEqual(expectedIdTransaction, transactions[0].Id);
        }

        [TestMethod]
        public async Task TransactionService_GetFilterTransactions_ThrowKeyNotFoundException()
        {
            int unexistMoneyAccountId = 5;
            TransactionFilters filters = new TransactionFilters
            {
                Direction = DirectionTransaction.OutputTransaction
            };

            await Assert.ThrowsExceptionAsync<KeyNotFoundException>(
                () => transactionService.GetFilterTransactions(unexistMoneyAccountId, filters));
        }

    }
}

﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Wallet.DAL.Entities;
using Wallet.DAL.RepositoryInterface;

namespace Wallet.BLL.Test.MockBD
{
    public class MockRepositoryUser: IRepositoryUser
    {
        private MockWalletDbContext dataBase;

        public MockRepositoryUser(MockWalletDbContext context)
        {
            dataBase = context;
        }

        public async Task<List<User>> GetAll()
        {
            return dataBase.Users;
        }

        public async Task<User?> GetById(int id)
        {
            return dataBase.Users.Where(t => t.Id == id).FirstOrDefault();
        }

        public async Task<User?> GetByLogin(string login)
        {
            return dataBase.Users.Where(t => t.Login == login).FirstOrDefault();
        }

        public async Task Add(User entity)
        {
            List<User> users = await GetAll();
            foreach (var user in users)
            {
                if (user.Login == entity.Login)
                    throw new DbUpdateException();
                if (user.PhoneNumber == entity.PhoneNumber)
                    throw new DbUpdateException();
            }

            dataBase.Users.Add(entity);
        }

        public void Delete(User entity)
        {
            dataBase.Users.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            User entity = dataBase.Users.Where(t => t.Id == id).FirstOrDefault();
            dataBase.Users.Remove(entity);
        }

        public void Update(User entity)
        {
            User updatedUser = dataBase.Users.Where(t => t.Id == entity.Id).FirstOrDefault();
            updatedUser = entity;
        }
    }
}

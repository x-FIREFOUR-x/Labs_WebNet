using System;
using System.Collections.Generic;

using Wallet.DAL.Entities;

namespace Wallet.BLL.Test.MockBD
{
    public class MockWalletDbContext
    {
        public List<User> Users { get; private set; }
        public List<MoneyAccount> MoneyAccounts { get; private set; }
        public List<Transaction> Transactions { get; private set; }

        public MockWalletDbContext()
        {
            Transactions = new List<Transaction>
            { 
                new Transaction() 
                {
                    Id = 1, Suma = 500, Type = "Travel", TimeImplementation = new DateTime(2021, 12, 1),
                    RecipientMoneyAccountId = 3, SenderMoneyAccountId = 1, RecipientMoneyAccount = null, SenderMoneyAccount = null
                },
                new Transaction()
                {
                    Id = 2, Suma = 1500, Type = "household appliances", TimeImplementation = new DateTime(2022, 2, 1),
                    RecipientMoneyAccountId = 1, SenderMoneyAccountId = 2, RecipientMoneyAccount = null, SenderMoneyAccount = null
                },
                new Transaction()
                {
                    Id = 3, Suma = 2500, Type = "Travel", TimeImplementation = new DateTime(2022, 4, 1),
                    RecipientMoneyAccountId = 1, SenderMoneyAccountId = 3, RecipientMoneyAccount = null, SenderMoneyAccount = null
                },

                new Transaction()
                {
                    Id = 4, Suma = 1000, Type = "", TimeImplementation = new DateTime(2022, 5, 1),
                    RecipientMoneyAccountId = 1, SenderMoneyAccountId = 2, RecipientMoneyAccount = null, SenderMoneyAccount = null
                },
                new Transaction()
                {
                    Id = 5, Suma = 200, Type = "Travel", TimeImplementation = new DateTime(2022, 11, 1),
                    RecipientMoneyAccountId = 2, SenderMoneyAccountId = 1, RecipientMoneyAccount = null, SenderMoneyAccount = null
                },

                new Transaction()
                {
                    Id = 6, Suma = 2500, Type = "Travel", TimeImplementation = new DateTime(2022, 11, 15),
                    RecipientMoneyAccountId = 2, SenderMoneyAccountId = 1, RecipientMoneyAccount = null, SenderMoneyAccount = null
                }
            };

            MoneyAccounts = new List<MoneyAccount>
            {
               new MoneyAccount()
               {
                   Id = 1, Balance = 15000, CardNumber ="99999999", ExpirationDate = new DateTime(2024, 12, 1),
                   UserId = 1, User = null, InputTransactions = null, OutputTransactions =null 
               },
               new MoneyAccount()
               {
                   Id = 2, Balance = 45000, CardNumber ="99123999", ExpirationDate = new DateTime(2023, 10, 1),
                   UserId = 1, User = null, InputTransactions = null, OutputTransactions =null
               },
               new MoneyAccount()
               {
                   Id = 3, Balance = 8000, CardNumber ="91234579", ExpirationDate = new DateTime(2020, 8, 1),
                   UserId = 2, User = null, InputTransactions = null, OutputTransactions =null
               }
            };

            Users = new List<User> 
            { 
                new User() {Id = 1, FirstName = "Олександр", LastName = "Грабов", PhoneNumber = "380670123456", Login = "User1", Password = "Password", MoneyAccounts = null},
                new User() {Id = 2, FirstName = "Павло", LastName = "Тропов", PhoneNumber = "380630654321", Login = "User2", Password = "Password", MoneyAccounts = null } 
            };
        }
    }
}

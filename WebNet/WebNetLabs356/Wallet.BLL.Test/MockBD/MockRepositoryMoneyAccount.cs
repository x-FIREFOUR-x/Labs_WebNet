﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Wallet.DAL.Entities;
using Wallet.DAL.RepositoryInterface;

namespace Wallet.BLL.Test.MockBD
{
    public class MockRepositoryMoneyAccount : IRepositoryMoneyAccount
    {
        private MockWalletDbContext dataBase;

        public MockRepositoryMoneyAccount(MockWalletDbContext context)
        {
            dataBase = context;
        }

        public async Task<List<MoneyAccount>> GetAll()
        {
            return dataBase.MoneyAccounts;
        }

        public async Task<MoneyAccount?> GetById(int id)
        {
            return dataBase.MoneyAccounts.Where(t => t.Id == id).FirstOrDefault();
        }

        public async Task<MoneyAccount?> GetByCardNumber(string cardNumber)
        {
            return dataBase.MoneyAccounts.Where(t => t.CardNumber == cardNumber).FirstOrDefault();
        }

        public async Task<List<MoneyAccount>> GetByUser(User user)
        {
            return dataBase.MoneyAccounts.Where(t => t.UserId == user.Id).ToList();
        }

        public async Task<List<MoneyAccount>> GetByUserId(int userId)
        {
            return dataBase.MoneyAccounts.Where(t => t.UserId == userId).ToList();
        }

        public async Task Add(MoneyAccount entity)
        {
            dataBase.MoneyAccounts.Add(entity);
        }

        public void Delete(MoneyAccount entity)
        {
            dataBase.MoneyAccounts.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            MoneyAccount entity = dataBase.MoneyAccounts.Where(t => t.Id == id).FirstOrDefault();
            dataBase.MoneyAccounts.Remove(entity);
        }

        public void Update(MoneyAccount entity)
        {
            MoneyAccount updatedMoneyAccount = dataBase.MoneyAccounts.Where(t => t.Id == entity.Id).FirstOrDefault();
            updatedMoneyAccount = entity;
        }
    }
}

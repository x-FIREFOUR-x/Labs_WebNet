﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Wallet.DAL.Entities;
using Wallet.DAL.RepositoryInterface;

namespace Wallet.BLL.Test.MockBD
{
    public class MockRepositoryTransaction : IRepositoryTransaction
    {
        private MockWalletDbContext dataBase;

        public MockRepositoryTransaction(MockWalletDbContext context)
        {
            dataBase = context;
        }

        public async Task<List<Transaction>> GetAll()
        {
            return dataBase.Transactions;
        }

        public async Task<Transaction?> GetById(int id)
        {
            return dataBase.Transactions.Where(t => t.Id == id).FirstOrDefault();
        }

        public async Task<List<Transaction>> GetByRecipientMoneyAccount(MoneyAccount moneyAccount)
        {
            return dataBase.Transactions.Where(t => t.RecipientMoneyAccountId == moneyAccount.Id).ToList();
        }

        public async Task<List<Transaction>> GetByRecipientMoneyAccountId(int moneyAccountId)
        {
            return dataBase.Transactions.Where(t => t.RecipientMoneyAccountId == moneyAccountId).ToList();
        }

        public async Task<List<Transaction>> GetBySenderMoneyAccount(MoneyAccount moneyAccount)
        {
            return dataBase.Transactions.Where(t => t.SenderMoneyAccountId == moneyAccount.Id).ToList();
        }

        public async Task<List<Transaction>> GetBySenderMoneyAccountId(int moneyAccountId)
        {
            return dataBase.Transactions.Where(t => t.SenderMoneyAccountId == moneyAccountId).ToList();
        }

        public async Task<List<Transaction>> GetByResipientMoneyAccountWhere(MoneyAccount moneyAccount, Expression<Func<Transaction, bool>> filter)
        {
            return dataBase.Transactions
                .Where(t => t.RecipientMoneyAccountId == moneyAccount.Id)
                .Where(filter.Compile())
                .ToList();
        }

        public async Task<List<Transaction>> GetByResipientMoneyAccountWhereId(int moneyAccountId, Expression<Func<Transaction, bool>> filter)
        {
            return dataBase.Transactions
                .Where(t => t.RecipientMoneyAccountId == moneyAccountId)
                .Where(filter.Compile())
                .ToList();
        }

        public async Task<List<Transaction>> GetBySenderMoneyAccountWhere(MoneyAccount moneyAccount, Expression<Func<Transaction, bool>> filter)
        {
            return dataBase.Transactions
                .Where(t => t.SenderMoneyAccountId == moneyAccount.Id)
                .Where(filter.Compile())
                .ToList();
        }

        public async Task<List<Transaction>> GetBySenderMoneyAccountWhereId(int moneyAccountId, Expression<Func<Transaction, bool>> filter)
        {
            return dataBase.Transactions
                .Where(t => t.SenderMoneyAccountId == moneyAccountId)
                .Where(filter.Compile())
                .ToList();
        }

        public async Task Add(Transaction entity)
        {
            dataBase.Transactions.Add(entity);
        }

        public void Delete(Transaction entity)
        {
            dataBase.Transactions.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            Transaction? entity = dataBase.Transactions.Where(t => t.Id == id).FirstOrDefault();
            dataBase.Transactions.Remove(entity);
        }

        public void Update(Transaction entity)
        {
            Transaction? updatedTransaction = dataBase.Transactions.Where(t => t.Id == entity.Id).FirstOrDefault();
            updatedTransaction = entity;
        }
    }
}

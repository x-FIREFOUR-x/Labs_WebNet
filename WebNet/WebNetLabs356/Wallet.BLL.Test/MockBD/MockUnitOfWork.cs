﻿using System;
using System.Threading.Tasks;

using Wallet.DAL.RepositoryInterface;

namespace Wallet.BLL.Test.MockBD
{
    public class MockUnitOfWork : IUnitOfWork
    {
        private readonly MockWalletDbContext DataBase;

        public MockUnitOfWork(MockWalletDbContext dataBase)
        {
            DataBase = dataBase;
            UserRepository = new MockRepositoryUser(DataBase);
            MoneyAccoutRepository = new MockRepositoryMoneyAccount(DataBase);
            TransactionRepository = new MockRepositoryTransaction(DataBase);
        }

        public IRepositoryUser UserRepository { get; }
        public IRepositoryMoneyAccount MoneyAccoutRepository { get; }
        public IRepositoryTransaction TransactionRepository { get; }

        public Task<int> SaveChangesAsync()
        {
            return Task.FromResult(0);
        }
    }
}

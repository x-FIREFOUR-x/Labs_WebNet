﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Wallet.BLL.Services;
using Wallet.BLL.Test.MockBD;

using Wallet.DAL.Entities;

namespace Wallet.BLL.Test
{
    [TestClass]
    public class MoneyAccountServiceTests
    {
        private static MoneyAccountService moneyAccountService;
        private static MockUnitOfWork unitOfWork;
        private static MockWalletDbContext dataBase;

        [TestInitialize]
        public void TestInitionalize()
        {
            dataBase = new MockWalletDbContext();
            unitOfWork = new MockUnitOfWork(dataBase);
            moneyAccountService = new MoneyAccountService(unitOfWork);
        }

        [TestMethod]
        public async Task MoneyAccountService_CreateNewMoneyAccount_SuccessOperation()
        {
            string login = "User1";
            User logedUser = await unitOfWork.UserRepository.GetByLogin(login);

            int oldCountMoneyAccounts = (await unitOfWork.MoneyAccoutRepository.GetByUser(logedUser)).Count;
            await moneyAccountService.CreateNewMoneyAccount(logedUser.Id);
            int newCountMoneyAccounts = (await unitOfWork.MoneyAccoutRepository.GetByUser(logedUser)).Count;

            Assert.AreEqual(newCountMoneyAccounts, oldCountMoneyAccounts + 1);
        }

        [TestMethod]
        public async Task MoneyAccountService_CreateNewMoneyAccount_ThrowKeyNotFoundException()
        {
            int UnexistId = 5;

            await Assert.ThrowsExceptionAsync<KeyNotFoundException>(
                () => moneyAccountService.CreateNewMoneyAccount(UnexistId));
        }


        [TestMethod]
        public async Task MoneyAccountService_GetListMoneyAccount_SuccessOperation()
        {
            string login = "User1";
            User logedUser = await unitOfWork.UserRepository.GetByLogin(login);
            List<MoneyAccount> expectedMoneyAccounts = dataBase.MoneyAccounts.Where(t => t.UserId == logedUser.Id).ToList();

            List<MoneyAccount> realMoneyAccounts = await moneyAccountService.GetListMoneyAccounts(logedUser.Id);

            Assert.AreEqual(expectedMoneyAccounts.Count, realMoneyAccounts.Count);
        }

        [TestMethod]
        public async Task MoneyAccountService_GetListMoneyAccount_ThrowKeyNotFoundException()
        {
            int UnexistId = 5;

            await Assert.ThrowsExceptionAsync<KeyNotFoundException>(
                () => moneyAccountService.GetListMoneyAccounts(UnexistId));
        }

    }
}

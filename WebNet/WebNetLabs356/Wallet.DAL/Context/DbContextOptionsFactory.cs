﻿using Microsoft.EntityFrameworkCore;

namespace Wallet.DAL.Context
{
    public class DbContextOptionsFactory
    {
        public static DbContextOptions<WalletDbContext> Get()
        {
            var builder = new DbContextOptionsBuilder<WalletDbContext>();

            builder.UseSqlServer("Data Source=(LocalDb)\\MSSQLLocalDB; database=WalletDb; Integrated Security=true",
                opt => opt.MigrationsAssembly(typeof(WalletDbContext).Assembly.GetName().Name));

            return builder.Options;
        }
    }
}

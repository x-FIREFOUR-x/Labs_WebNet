﻿using Microsoft.EntityFrameworkCore;

using Wallet.DAL.Entities;

namespace Wallet.DAL.Context
{
    public class WalletDbContext: DbContext
    {
        public DbSet<User> Users { get; private set; }
        public DbSet<MoneyAccount> MoneyAccounts { get; private set; }
        public DbSet<Transaction> Transactions { get; private set; }

        public WalletDbContext(DbContextOptions<WalletDbContext> options)
        : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
            .HasIndex(p => new {p.Login}).IsUnique();

            modelBuilder.Entity<User>()
           .HasIndex(p => new {p.PhoneNumber }).IsUnique();

            modelBuilder.Entity<MoneyAccount>()
           .HasIndex(p => new { p.CardNumber }).IsUnique();

            modelBuilder.Entity<User>()
            .HasMany(c => c.MoneyAccounts)
            .WithOne(c => c.User)
            .HasForeignKey(c => c.UserId)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<MoneyAccount>()
            .HasMany(c => c.InputTransactions)
            .WithOne(u => u.RecipientMoneyAccount)
            .HasForeignKey(c => c.RecipientMoneyAccountId)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<MoneyAccount>()
            .HasMany(c => c.OutputTransactions)
            .WithOne(u => u.SenderMoneyAccount)
            .HasForeignKey(c => c.SenderMoneyAccountId)
            .OnDelete(DeleteBehavior.Restrict);
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Wallet.DAL.Entities;

namespace Wallet.DAL.RepositoryInterface
{
    public interface IRepositoryUser
    {
        Task<List<User>> GetAll();

        Task<User?> GetById(int id);

        Task<User?> GetByLogin(string login);

        Task Add(User entity);

        void Delete(User entity);

        Task DeleteById(int id);

        void Update(User entity);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Wallet.DAL.Entities;

namespace Wallet.DAL.RepositoryInterface
{
    public interface IRepositoryMoneyAccount
    {
        Task<List<MoneyAccount>> GetAll();

        Task<MoneyAccount?> GetById(int id);

        Task<MoneyAccount?> GetByCardNumber(string cardNumber);

        Task<List<MoneyAccount>> GetByUser(User user);

        Task<List<MoneyAccount>> GetByUserId(int userId);

        Task Add(MoneyAccount entity);

        void Delete(MoneyAccount entity);

        Task DeleteById(int id);

        void Update(MoneyAccount entity);
    }
}

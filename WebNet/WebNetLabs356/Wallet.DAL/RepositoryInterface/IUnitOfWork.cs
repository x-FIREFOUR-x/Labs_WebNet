﻿using System.Threading.Tasks;

using Wallet.DAL.Repository;

namespace Wallet.DAL.RepositoryInterface
{
    public interface IUnitOfWork
    {
        IRepositoryUser UserRepository { get; }
        IRepositoryMoneyAccount MoneyAccoutRepository { get; }
        IRepositoryTransaction TransactionRepository { get; }
        
        Task<int> SaveChangesAsync();
    }
}

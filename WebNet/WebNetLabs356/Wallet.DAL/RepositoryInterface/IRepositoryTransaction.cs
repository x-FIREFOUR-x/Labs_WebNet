﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Wallet.DAL.Entities;

namespace Wallet.DAL.RepositoryInterface
{
    public interface IRepositoryTransaction
    {
        Task<List<Transaction>> GetAll();

        Task<Transaction?> GetById(int id);

        Task<List<Transaction>> GetByRecipientMoneyAccount(MoneyAccount moneyAccount);

        Task<List<Transaction>> GetByRecipientMoneyAccountId(int moneyAccountId);

        Task<List<Transaction>> GetBySenderMoneyAccount(MoneyAccount moneyAccount);

        Task<List<Transaction>> GetBySenderMoneyAccountId(int moneyAccountId);

        Task<List<Transaction>> GetByResipientMoneyAccountWhere(MoneyAccount moneyAccount, Expression<Func<Transaction, bool>> filter);

        Task<List<Transaction>> GetByResipientMoneyAccountWhereId(int moneyAccountId, Expression<Func<Transaction, bool>> filter);

        Task<List<Transaction>> GetBySenderMoneyAccountWhere(MoneyAccount moneyAccount, Expression<Func<Transaction, bool>> filter);

        Task<List<Transaction>> GetBySenderMoneyAccountWhereId(int moneyAccountId, Expression<Func<Transaction, bool>> filter);

        Task Add(Transaction entity);

        void Delete(Transaction entity);

        Task DeleteById(int id);

        void Update(Transaction entity);
    }
}

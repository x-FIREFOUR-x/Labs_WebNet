﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using Wallet.DAL.Context;
using Wallet.DAL.Entities;
using Wallet.DAL.RepositoryInterface;

namespace Wallet.DAL.Repository
{
    public class RepositoryUser : IRepositoryUser
    {
        private readonly WalletDbContext dataBase;

        public RepositoryUser(WalletDbContext context)
        {
            dataBase = context;
        }

        public async Task<List<User>> GetAll()
        {
            return await dataBase.Users.ToListAsync();
        }

        public async Task<User?> GetById(int id)
        {
            return await dataBase.Users.FindAsync(id).AsTask();
        }

        public async Task<User?> GetByLogin(string login)
        {
            return await dataBase.Users.Where(t => t.Login == login).FirstOrDefaultAsync();
        }

        public async Task Add(User entity)
        {
            await dataBase.Users.AddAsync(entity);
        }

        public void Delete(User entity)
        {
            dataBase.Users.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            User? entity = await GetById(id);
            Delete(entity);
        }

        public void Update(User entity)
        {
            dataBase.Users.Attach(entity);
            dataBase.Entry(entity).State = EntityState.Modified;
        }
    }
}

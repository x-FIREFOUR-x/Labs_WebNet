﻿using System.Threading.Tasks;

using Wallet.DAL.Context;
using Wallet.DAL.RepositoryInterface;

namespace Wallet.DAL.Repository
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly WalletDbContext DataBase;

        public UnitOfWork(WalletDbContext dataBase)
        {
            DataBase = dataBase;
            UserRepository = new RepositoryUser(DataBase);
            MoneyAccoutRepository = new RepositoryMoneyAccount(DataBase);
            TransactionRepository = new RepositoryTransaction(DataBase);
        }

        public IRepositoryUser UserRepository { get; }
        public IRepositoryMoneyAccount MoneyAccoutRepository { get; }
        public IRepositoryTransaction TransactionRepository { get; }

        public Task<int> SaveChangesAsync()
        {
            return DataBase.SaveChangesAsync();
        }
    }
}

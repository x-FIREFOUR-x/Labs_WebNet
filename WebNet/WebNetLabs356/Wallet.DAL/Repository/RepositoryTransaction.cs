﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Wallet.DAL.Context;
using Wallet.DAL.Entities;
using Wallet.DAL.RepositoryInterface;

namespace Wallet.DAL.Repository
{
    public class RepositoryTransaction : IRepositoryTransaction
    {
        private readonly WalletDbContext dataBase;

        public RepositoryTransaction(WalletDbContext context)
        {
            dataBase = context;
        }

        public async Task<List<Transaction>> GetAll()
        {
            return await dataBase.Transactions.ToListAsync();
        }

        public async Task<Transaction?> GetById(int id)
        {
            return await dataBase.Transactions.FindAsync(id).AsTask();
        }

        public async Task<List<Transaction>> GetByRecipientMoneyAccount(MoneyAccount moneyAccount)
        {
            return await dataBase.Transactions.Where(t => t.RecipientMoneyAccountId == moneyAccount.Id).ToListAsync();
        }

        public async Task<List<Transaction>> GetByRecipientMoneyAccountId(int moneyAccountId)
        {
            return await dataBase.Transactions.Where(t => t.RecipientMoneyAccountId == moneyAccountId).ToListAsync();
        }

        public async Task<List<Transaction>> GetBySenderMoneyAccount(MoneyAccount moneyAccount)
        {
            return await dataBase.Transactions.Where(t => t.SenderMoneyAccountId == moneyAccount.Id).ToListAsync();
        }

        public async Task<List<Transaction>> GetBySenderMoneyAccountId(int moneyAccountId)
        {
            return await dataBase.Transactions.Where(t => t.SenderMoneyAccountId == moneyAccountId).ToListAsync();
        }

        public async Task<List<Transaction>> GetByResipientMoneyAccountWhere(MoneyAccount moneyAccount, Expression<Func<Transaction, bool>> filter)
        {
            return await dataBase.Transactions
                .Include(transaction => transaction.RecipientMoneyAccount)
                .Include(transaction => transaction.SenderMoneyAccount)
                .Where(t => t.RecipientMoneyAccountId == moneyAccount.Id)
                .Where(filter)
                .ToListAsync();
        }

        public async Task<List<Transaction>> GetByResipientMoneyAccountWhereId(int moneyAccountId, Expression<Func<Transaction, bool>> filter)
        {
            return await dataBase.Transactions
                .Include(transaction => transaction.RecipientMoneyAccount)
                .Include(transaction => transaction.SenderMoneyAccount)
                .Where(t => t.RecipientMoneyAccountId == moneyAccountId)
                .Where(filter)
                .ToListAsync();
        }

        public async Task<List<Transaction>> GetBySenderMoneyAccountWhere(MoneyAccount moneyAccount, Expression<Func<Transaction, bool>> filter)
        {
            return await dataBase.Transactions
                .Include(transaction => transaction.RecipientMoneyAccount)
                .Include(transaction => transaction.SenderMoneyAccount)
                .Where(t => t.SenderMoneyAccountId == moneyAccount.Id)
                .Where(filter)
                .ToListAsync();
        }

        public async Task<List<Transaction>> GetBySenderMoneyAccountWhereId(int moneyAccountId, Expression<Func<Transaction, bool>> filter)
        {
            return await dataBase.Transactions
                .Include(transaction => transaction.RecipientMoneyAccount)
                .Include(transaction => transaction.SenderMoneyAccount)
                .Where(t => t.SenderMoneyAccountId == moneyAccountId)
                .Where(filter)
                .ToListAsync();
        }

        public async Task Add(Transaction entity)
        {
            await dataBase.Transactions.AddAsync(entity);
        }

        public void Delete(Transaction entity)
        {
            dataBase.Transactions.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            Transaction? entity = await GetById(id);
            Delete(entity);
        }

        public void Update(Transaction entity)
        {
            dataBase.Transactions.Attach(entity);
            dataBase.Entry(entity).State = EntityState.Modified;
        }
    }
}

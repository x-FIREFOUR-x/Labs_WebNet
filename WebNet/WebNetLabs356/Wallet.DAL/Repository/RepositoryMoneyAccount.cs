﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Wallet.DAL.Context;
using Wallet.DAL.Entities;
using Wallet.DAL.RepositoryInterface;

namespace Wallet.DAL.Repository
{
    public class RepositoryMoneyAccount: IRepositoryMoneyAccount
    {
        private readonly WalletDbContext dataBase;

        public RepositoryMoneyAccount(WalletDbContext context)
        {
            dataBase = context;
        }

        public async Task<List<MoneyAccount>> GetAll()
        {
            return await dataBase.MoneyAccounts.ToListAsync();
        }

        public async Task<MoneyAccount?> GetById(int id)
        {
            return await dataBase.MoneyAccounts.FindAsync(id).AsTask();
        }

        public async Task<MoneyAccount?> GetByCardNumber(string cardNumber)
        {
            return await dataBase.MoneyAccounts.Where(t => t.CardNumber == cardNumber).FirstOrDefaultAsync();
        }

        public async Task<List<MoneyAccount>> GetByUser(User user)
        {
            return await dataBase.MoneyAccounts.Where(t => t.UserId == user.Id).ToListAsync();
        }

        public async Task<List<MoneyAccount>> GetByUserId(int userId)
        {
            return await dataBase.MoneyAccounts.Where(t => t.UserId == userId).ToListAsync();
        }

        public async Task Add(MoneyAccount entity)
        {
            await dataBase.MoneyAccounts.AddAsync(entity);
        }

        public void Delete(MoneyAccount entity)
        {
            dataBase.MoneyAccounts.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            MoneyAccount? entity = await GetById(id);
            Delete(entity);
        }

        public void Update(MoneyAccount entity)
        {
            dataBase.MoneyAccounts.Attach(entity);
            dataBase.Entry(entity).State = EntityState.Modified;
        }
    }
}

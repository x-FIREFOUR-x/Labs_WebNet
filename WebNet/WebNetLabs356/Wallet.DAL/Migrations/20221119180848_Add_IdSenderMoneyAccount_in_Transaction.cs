﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wallet.DAL.Migrations
{
    public partial class Add_IdSenderMoneyAccount_in_Transaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MoneyAccounts_MoneyAccountId",
                table: "Transactions");

            migrationBuilder.RenameColumn(
                name: "MoneyAccountId",
                table: "Transactions",
                newName: "RecipientMoneyAccountId");

            migrationBuilder.RenameIndex(
                name: "IX_Transactions_MoneyAccountId",
                table: "Transactions",
                newName: "IX_Transactions_RecipientMoneyAccountId");

            migrationBuilder.AddColumn<int>(
                name: "IdSenderMoneyAccount",
                table: "Transactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions",
                column: "RecipientMoneyAccountId",
                principalTable: "MoneyAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "IdSenderMoneyAccount",
                table: "Transactions");

            migrationBuilder.RenameColumn(
                name: "RecipientMoneyAccountId",
                table: "Transactions",
                newName: "MoneyAccountId");

            migrationBuilder.RenameIndex(
                name: "IX_Transactions_RecipientMoneyAccountId",
                table: "Transactions",
                newName: "IX_Transactions_MoneyAccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MoneyAccounts_MoneyAccountId",
                table: "Transactions",
                column: "MoneyAccountId",
                principalTable: "MoneyAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wallet.DAL.Migrations
{
    public partial class ChangeBD : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MoneyAccounts_Users_Userid",
                table: "MoneyAccounts");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Users",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "Userid",
                table: "MoneyAccounts",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_MoneyAccounts_Userid",
                table: "MoneyAccounts",
                newName: "IX_MoneyAccounts_UserId");

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CardNumber",
                table: "MoneyAccounts",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ExpirationDate",
                table: "MoneyAccounts",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CardNumber",
                table: "MoneyAccounts");

            migrationBuilder.DropColumn(
                name: "ExpirationDate",
                table: "MoneyAccounts");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Users",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "MoneyAccounts",
                newName: "Userid");

            migrationBuilder.RenameIndex(
                name: "IX_MoneyAccounts_UserId",
                table: "MoneyAccounts",
                newName: "IX_MoneyAccounts_Userid");

            migrationBuilder.AddForeignKey(
                name: "FK_MoneyAccounts_Users_Userid",
                table: "MoneyAccounts",
                column: "Userid",
                principalTable: "Users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

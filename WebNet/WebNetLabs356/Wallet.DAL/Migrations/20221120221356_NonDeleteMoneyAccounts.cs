﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wallet.DAL.Migrations
{
    public partial class NonDeleteMoneyAccounts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MoneyAccounts_SenderMoneyAccountId",
                table: "Transactions");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions",
                column: "RecipientMoneyAccountId",
                principalTable: "MoneyAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MoneyAccounts_SenderMoneyAccountId",
                table: "Transactions",
                column: "SenderMoneyAccountId",
                principalTable: "MoneyAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MoneyAccounts_SenderMoneyAccountId",
                table: "Transactions");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions",
                column: "RecipientMoneyAccountId",
                principalTable: "MoneyAccounts",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MoneyAccounts_SenderMoneyAccountId",
                table: "Transactions",
                column: "SenderMoneyAccountId",
                principalTable: "MoneyAccounts",
                principalColumn: "Id");
        }
    }
}

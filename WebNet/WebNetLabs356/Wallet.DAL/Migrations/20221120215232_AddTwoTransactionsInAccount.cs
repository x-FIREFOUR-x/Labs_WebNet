﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wallet.DAL.Migrations
{
    public partial class AddTwoTransactionsInAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions");

            migrationBuilder.RenameColumn(
                name: "IdSenderMoneyAccount",
                table: "Transactions",
                newName: "SenderMoneyAccountId");

            migrationBuilder.AlterColumn<int>(
                name: "RecipientMoneyAccountId",
                table: "Transactions",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_SenderMoneyAccountId",
                table: "Transactions",
                column: "SenderMoneyAccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions",
                column: "RecipientMoneyAccountId",
                principalTable: "MoneyAccounts",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MoneyAccounts_SenderMoneyAccountId",
                table: "Transactions",
                column: "SenderMoneyAccountId",
                principalTable: "MoneyAccounts",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MoneyAccounts_SenderMoneyAccountId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_SenderMoneyAccountId",
                table: "Transactions");

            migrationBuilder.RenameColumn(
                name: "SenderMoneyAccountId",
                table: "Transactions",
                newName: "IdSenderMoneyAccount");

            migrationBuilder.AlterColumn<int>(
                name: "RecipientMoneyAccountId",
                table: "Transactions",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MoneyAccounts_RecipientMoneyAccountId",
                table: "Transactions",
                column: "RecipientMoneyAccountId",
                principalTable: "MoneyAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

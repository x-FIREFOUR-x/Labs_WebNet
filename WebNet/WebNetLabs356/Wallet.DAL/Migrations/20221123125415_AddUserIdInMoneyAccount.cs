﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wallet.DAL.Migrations
{
    public partial class AddUserIdInMoneyAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "MoneyAccounts",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "MoneyAccounts",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

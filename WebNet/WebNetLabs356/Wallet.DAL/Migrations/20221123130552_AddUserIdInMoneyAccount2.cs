﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wallet.DAL.Migrations
{
    public partial class AddUserIdInMoneyAccount2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts");

            migrationBuilder.AddForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts");

            migrationBuilder.AddForeignKey(
                name: "FK_MoneyAccounts_Users_UserId",
                table: "MoneyAccounts",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

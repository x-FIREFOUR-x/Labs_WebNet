﻿using System;
using System.Collections.Generic;

namespace Wallet.DAL.Entities
{
    public class MoneyAccount
    {
        public int Id { get; set; }
        public double Balance { get; set; }
        public string CardNumber { get; set; }
        public DateTime ExpirationDate { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public ICollection<Transaction> InputTransactions { get; set; }
        public ICollection<Transaction> OutputTransactions { get; set; }
        public MoneyAccount() 
        {
            InputTransactions = new List<Transaction>();
            OutputTransactions = new List<Transaction>();
        }

    }
}

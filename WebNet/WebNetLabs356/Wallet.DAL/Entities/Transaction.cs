﻿using System;

namespace Wallet.DAL.Entities
{
    public class Transaction
    {
        public int Id { get; set; }
        public double Suma { get; set; }
        public string? Type { get; set; }
        public DateTime TimeImplementation { get; set; }

        public MoneyAccount RecipientMoneyAccount { get; set; }
        public MoneyAccount SenderMoneyAccount { get; set; }

        public int RecipientMoneyAccountId { get; set; }
        public int SenderMoneyAccountId { get; set; }
    }
}

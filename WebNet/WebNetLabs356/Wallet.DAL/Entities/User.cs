﻿using System.Collections.Generic;

namespace Wallet.DAL.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName{ get; set;}
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public ICollection<MoneyAccount> MoneyAccounts { get; set; }


        public User()
        {
            MoneyAccounts = new List<MoneyAccount>();
        }
    }
}

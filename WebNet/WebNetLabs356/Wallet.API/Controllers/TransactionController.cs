﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using AutoMapper;
using System.Threading.Tasks;

using Wallet.BLL.ServicesInterface;
using Wallet.BLL.DTO;

namespace Wallet.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;
        private readonly IMapper _mapper;

        public TransactionController(ITransactionService transactionService, IMapper mapper)
        {
            _transactionService = transactionService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<TransactionData>> Create(
            [FromQuery] int moneyAccountId,
            [FromBody] CreateTransactionData transactionData)
        {
            return Ok(_mapper.Map<TransactionData>(
                await _transactionService.CreateTransaction(moneyAccountId, transactionData)));
        }

        [HttpGet("InputTransactions")]
        public async Task<ActionResult<List<TransactionData>>> GetInput([FromQuery] int moneyAccountId)
        {
            return Ok(_mapper.Map<List<TransactionData>>(
                await _transactionService.GetInputTransactions(moneyAccountId)));
        }

        [HttpGet("OutputTransactions")]
        public async Task<ActionResult<List<TransactionData>>> GetOutput([FromQuery] int moneyAccountId)
        {
            return Ok(_mapper.Map<List<TransactionData>>(
                await _transactionService.GetOutputTransactions(moneyAccountId)));
        }

        [HttpGet("Transactions")]
        public async Task<ActionResult<List<TransactionData>>> GetTransaction([FromQuery] int moneyAccountId)
        {
            return Ok(_mapper.Map<List<TransactionData>>(
                await _transactionService.GetTransactions(moneyAccountId)));
        }

        [HttpGet("FilteredTransactions")]
        public async Task<ActionResult<List<TransactionData>>> GetTransactionFilter(
            [FromQuery] int moneyAccountId,
            [FromQuery] TransactionFilters filters)
        {
            return Ok(_mapper.Map<List<TransactionData>>(
                await _transactionService.GetFilterTransactions(moneyAccountId, filters)));
        }
    }
}

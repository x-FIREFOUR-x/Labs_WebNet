﻿using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.Threading.Tasks;

using Wallet.BLL.ServicesInterface;
using Wallet.BLL.DTO;

namespace Wallet.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost("Registration")]
        async public Task<ActionResult<UserData>> Create([FromBody] UserRegistrationData userData)
        {
           return Ok(_mapper.Map<UserData>(await _userService.CreateUser(userData)));
        }

        [HttpPost("Login")]
        async public Task<ActionResult<UserData>> Login([FromBody] UserLoginData userData)
        {
            return Ok(_mapper.Map<UserData>(await _userService.LoginUser(userData)));
        }

    }
}

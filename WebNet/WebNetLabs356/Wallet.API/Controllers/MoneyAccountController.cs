﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

using Wallet.BLL.ServicesInterface;
using Wallet.BLL.DTO;

using Wallet.DAL.Entities;

namespace Wallet.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MoneyAccountController : ControllerBase
    {
        private readonly IMoneyAccountService _moneyAccountService;
        private readonly IMapper _mapper;

        public MoneyAccountController(IMoneyAccountService moneyAccountService, IMapper mapper)
        {
            _moneyAccountService = moneyAccountService;
            _mapper = mapper;
        }

        
        [HttpPost]
        async public Task<ActionResult<MoneyAccountData>> Create([FromQuery] int userId)
        {
            return Ok(_mapper.Map<MoneyAccountData>(await _moneyAccountService.CreateNewMoneyAccount(userId)));
        }

        [HttpGet]
        async public Task<ActionResult<List<MoneyAccount>>> Get([FromQuery] int userId)
        {
            return Ok(_mapper.Map<List<MoneyAccountData>>(await _moneyAccountService.GetListMoneyAccounts(userId)));
        }

    }
}

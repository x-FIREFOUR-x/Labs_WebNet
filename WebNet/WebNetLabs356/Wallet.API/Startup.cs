using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using Wallet.BLL.Services;
using Wallet.BLL.ServicesInterface;

using Wallet.DAL.Context;
using Wallet.DAL.Repository;
using Wallet.DAL.RepositoryInterface;

using Wallet.API.Middleware;

namespace Wallet.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Wallet.API", Version = "v1" });
            });

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IMoneyAccountService, MoneyAccountService>();
            services.AddScoped<ITransactionService, TransactionService>();

            var migrationAssembly = typeof(WalletDbContext).Assembly.GetName().Name;
            services.AddDbContext<WalletDbContext>(options =>
                options.UseSqlServer(Configuration["ConnectionStrings:WalletDBConnection"],
                    opt => opt.MigrationsAssembly(migrationAssembly)));

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IRepositoryUser, RepositoryUser>();
            services.AddScoped<IRepositoryMoneyAccount, RepositoryMoneyAccount>();
            services.AddScoped<IRepositoryTransaction, RepositoryTransaction>();

            services.AddSingleton<IMapper, EntityMapper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Wallet.API v1"));
            }

            app.UseMiddleware<ExceptionHandlerMiddleware>();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

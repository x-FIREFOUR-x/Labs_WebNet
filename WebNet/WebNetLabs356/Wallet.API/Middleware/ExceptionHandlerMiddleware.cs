﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Text.Json;
using System.Threading.Tasks;

using Wallet.BLL.Exceptions;

namespace Wallet.API.Middleware
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _delegate;

        public ExceptionHandlerMiddleware(RequestDelegate requestDelegate)
        {
            _delegate = requestDelegate;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _delegate(context);
            }
            catch (Exception e)
            {
                await MapException(context, e);
            }
        }

        private static Task MapException(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = exception switch
            {
                AlreadyExistData => 400,
                AuthenticationException => 400,
                InsufficientFundsException => 400,
                ArgumentException => 400,
                KeyNotFoundException => 404,
                _ => 500,
            };

            var json = JsonSerializer.Serialize(exception.Message);

            return context.Response.WriteAsync(json);
        }

    }
}

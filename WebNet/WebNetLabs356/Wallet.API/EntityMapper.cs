﻿using AutoMapper;
using Wallet.BLL.DTO;
using Wallet.DAL.Entities;

namespace Wallet.API
{
    public class EntityMapper : Mapper
    {
        public EntityMapper() : base(new MapperConfiguration(EntityMapper.Configuration)) { /* default */ }

        private static void Configuration(IMapperConfigurationExpression config)
        {
            config.CreateMap<Transaction, TransactionData>();

            config.CreateMap<MoneyAccount, MoneyAccountData>();

            config.CreateMap<User, UserData>();
        }
    }
}

